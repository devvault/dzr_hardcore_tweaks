class CfgPatches
{
	class dzr_rag_craft
	{
		requiredAddons[] = {"DZ_Data", "DZ_Characters"};
		units[] = {"dzr_Bandana_rag"};
		weapons[] = {};
	};
};
class CfgMods
{
	class dzr_rag_craft
	{
		type = "mod";
		author = "DayZ Russia";
		dir = "dzr_rag_craft";
		name = "dzr_rag_craft";
		dependencies[] = {"World"};
		class defs
		{
			class worldScriptModule
			{
				files[] = {"dzr_rag_craft/4_World"};
			};
		};
	};
};
class CfgVehicles
{
	class Bandana_ColorBase;
	class dzr_Bandana_rag: Bandana_ColorBase
	{
		scope = 2;
		hiddenSelectionsTextures[] = {"dzr_rag_craft\data\Bandana_rag_co.paa","dzr_rag_craft\data\Bandana_rag_co.paa","dzr_rag_craft\data\Bandana_rag_co.paa","dzr_rag_craft\data\Bandana_rag_co.paa","dzr_rag_craft\data\Bandana_rag_co.paa"};
	};
};